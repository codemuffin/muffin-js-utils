import { Muffin_VariableChecks } from './Muffin_VariableChecks';

class Muffin_QueryString
{
	parseQueryStringToObject( debugLog = false )
	{
		let queryString = '';
		let queryStringPairsArr = [];
		let queryStringSplitArr = [];
		let queryStringSplitObj = [];

		queryString = window.location.search;

		// Ignore empty, or just "?"
		if ( queryString.length < 2 )
		{
			return null;
		}

		queryString = ( queryString.startsWith( '?' ) ) ? queryString.substr( 1, queryString.length ) : '';

		queryStringPairsArr = queryString.split( '&' );

		queryStringPairsArr.forEach( pair =>
		{
			const splitPair = pair.split( '=' );

			if ( splitPair.length )
			{
				const pairKey = splitPair[0];
				const pairVal = splitPair[1];

				queryStringSplitArr.push( splitPair ); // UNUSED

				queryStringSplitObj[pairKey] = pairVal;
			}
		} );

		if ( debugLog )
		{
			console.log( { queryString, queryStringPairsArr, queryStringSplitArr, queryStringSplitObj } );
		}

		return queryStringSplitObj;
	}

	queryStringContainsAllKeys( keysArr = [], debugLog = false )
	{
		// Parsed query string object
		let qsObj = this.parseQueryStringToObject( debugLog );

		if ( !qsObj )
		{
			return;
		}

		const VC = new Muffin_VariableChecks();

		return VC.objectContainsAllKeys( qsObj, keysArr, debugLog );
	}
}

export { Muffin_QueryString };
