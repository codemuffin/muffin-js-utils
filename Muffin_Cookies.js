class MuffinCookies
{
	setCookie( key, value )
	{
		const keyValue = `${key}=${value}`;
		const expiry   = new Date( 'January 1, 2030' ).toUTCString();
		const path     = 'path=/';

		const cookieString = [ keyValue, expiry, path ].join( ';' );

		document.cookie = cookieString;
	}

	getCookieValue( key )
	{
		const cookiesSplit = document.cookie.split( ';' );

		for ( let i = 0; i < cookiesSplit.length; i++ )
		{
			const pair = cookiesSplit[i].trim(); // trim whitespace after each ";"

			if ( pair.indexOf( key + '=' ) === 0 )
			{
				return pair.split( '=' )[1];
			}
		}

		return undefined;
	}
}

export { MuffinCookies };
