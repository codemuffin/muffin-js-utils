import { MuffinCookies } from './Muffin_Cookies';

/**
 * Toggle a body class `darkmode`
 *
 * Listens for clicks on `.darkmode-toggle-btn'`
 */
class MuffinDarkMode
{
	init()
	{
		this.toggleBtn();
		this.setInitial();
	}

	toggleBtn()
	{
		document.querySelector( '.darkmode-toggle-btn' ).addEventListener( 'click', () =>
		{
			if ( document.body.classList.contains( 'darkmode' ) )
			{
				this.toggleDarkMode( false );
			}
			else
			{
				this.toggleDarkMode( true );
			}
		});
	}

	/**
	 * Toggle dark mode, on or off
	 *
	 * @param   {bool}  state  True to turn dark mode ON, false to turn if off
	 *
	 * @return  {void}
	 */
	toggleDarkMode( state )
	{
		switch( state )
		{
			case true:
				document.body.classList.add( 'darkmode' );
				MuffinCookies.setCookie( 'darkmode', 'ON' );
				break;

			case false:
				document.body.classList.remove( 'darkmode' );
				MuffinCookies.setCookie( 'darkmode', 'OFF' );
				break;
		}
	}

	setInitial()
	{
		if ( MuffinCookies.getCookieValue( 'darkmode' ) === 'ON' )
		{
			this.toggleDarkMode( true );
		}
		else
		{
			this.toggleDarkMode( false );
		}
	}
}

export { MuffinDarkMode };
