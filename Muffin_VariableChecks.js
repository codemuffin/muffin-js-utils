class Muffin_VariableChecks
{
	arrayIsValid( checkArr, debugLog = false )
	{
		let argIsValid = true;
		let debugMsg = '';

		if ( argIsValid && !checkArr )
		{
			argIsValid = false;
			debugMsg = 'Supplied array arg was undefined/null/false';
		}

		if ( argIsValid && !Array.isArray( checkArr ) )
		{
			argIsValid = false;
			debugMsg = `Expected array but supplied arg type was: ${typeof checkArr}`;
		}

		if ( argIsValid && checkArr.length < 1 )
		{
			argIsValid = false;
			debugMsg = 'Supplied array was empty';
		}

		// Debug Log
		if ( !argIsValid && debugLog )
		{
			console.log( `[arrayIsValid] Validation fail: ${debugMsg}` );
		}

		return argIsValid;
	}

	objectIsValid( checkObj, debugLog = false )
	{
		let argIsValid = true;
		let debugMsg = '';

		if ( argIsValid && !checkObj )
		{
			argIsValid = false;
			debugMsg = 'Supplied array arg was undefined/null/false';
		}

		if ( argIsValid && typeof checkObj !== 'object' )
		{
			argIsValid = false;
			debugMsg = `Expected object but supplied arg type was: ${typeof checkArr}`;
		}

		if ( argIsValid && !Object.keys( checkObj ).length )
		{
			argIsValid = false;
			debugMsg = 'Supplied object was empty';
		}

		// Debug Log
		if ( !argIsValid && debugLog )
		{
			console.log( `[objectIsValid] Validation fail: ${debugMsg}` );
		}

		return argIsValid;
	}

	objectContainsAllKeys( checkObj = {}, checkKeysArr = [], debugLog = false )
	{
		// Validation block
		const debugMsgLabel = '[objectContainsAllKeys]';

		if ( !this.objectIsValid( checkObj, debugLog ) )
		{
			if ( debugLog )
			{
				console.log( `${debugMsgLabel} Validation error for 1st arg (object), was supplied:`, checkObj );
			}

			return false;
		}

		if ( !this.arrayIsValid( checkKeysArr, debugLog ) )
		{
			if ( debugLog )
			{
				console.log( `${debugMsgLabel} Validation error for 2nd arg (array), was supplied:`, checkKeysArr );
			}

			return false;
		}

		// Actual check
		let containsKey = true;
		let checkKey = '';

		for ( let arrInd = 0; arrInd < checkKeysArr.length; arrInd++ )
		{
			checkKey = checkKeysArr[arrInd];

			if ( !Object.prototype.hasOwnProperty.call( checkObj, checkKey ) )
			{
				containsKey = false;
				break;
			}
		}

		return containsKey;
	}
}

export { Muffin_VariableChecks };
